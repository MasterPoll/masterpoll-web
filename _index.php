<?php
	error_reporting(0);
	if (!isset($_SESSION)) {
		include("/home/masterpoll-documents/website/file_names.php");
		$error = 401;
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: 401 Access denied");
		} else {
			header("HTTP/1.1 401 Access denied");
		}
		http_response_code(401);
		@require($f['display_web_errors']);
		die;
	}
	if ($_GET['logout']) {
		setcookie('tg_user', '', 0, '/', "masterpoll.xyz");
		unset($_SESSION['tg_user']);
		unset($_COOKIE['tg_user']);
		unset($_COOKIE['expires']);
		if ($_GET['logout'] == "login_index") {
			$url = "https://web.masterpoll.xyz";
		} elseif ($_GET['logout'] == "index") {
			$url = "https://masterpoll.xyz";
		} elseif ($_GET['logout'] == "translations") {
			$url = "https://web.masterpoll.xyz/translations/";
		} else {
			$url = "https://masterpoll.xyz/thread.php?p=" . $_GET['logout'];
		}
		echo '<meta http-equiv="refresh" content="0;URL=' . $url . '">';
		die;
	}
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<title>Telegram Login for Master Poll</title>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans&amp;display=swap">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131336731-7"></script>
		<script>
  			window.dataLayer = window.dataLayer || [];
  			function gtag(){dataLayer.push(arguments);}
  			gtag('js', new Date());
  			gtag('config', 'UA-131336731-7');
		</script>
	</head>
	<body>
		<?php
		if ($_GET['bot']) {
			$cloni = ["MasterPollBot", "MasterPoll2Bot", "NewGroupAgreeBot", "MasterPollBetaBot"];
			if (!in_array($_GET['bot'], $cloni)) {
				$bot = "MasterPollBot";
			} else {
				$bot = $_GET['bot'];
			}
		} else {
			if (!isset($_SESSION['bot'])) {
				$bot = "MasterPollBot";
			} else {
				$bot = $_SESSION['bot'];
			}
		}
		$_SESSION['bot'] = $bot;
		if ($_GET['thread']) {
			$_SESSION['thread'] = $_GET['thread'];
		}
		?>
		<style>
			@font-face {
				font-family: 'Open Sans', sans-serif;
			}

			a {
				color: rgb(0, 160, 255);
				text-decoration: none;
			}
			
			a:hover {
				color: rgb(0, 110, 205);
			}

			.login {
				display: -webkit-flexbox;
				display: -ms-flexbox;
				display: -webkit-flex;
				display: flex;
				-webkit-flex-align: center;
				-ms-flex-align: center;
				-webkit-align-items: center;
				align-items: center;
				justify-content: center;
				font-family: 'Open Sans', sans-serif;
			}
			
			.button {
				background-color: #3aa2f8;
				border: none;
				color: white;
				padding: 10px 20px;
				text-align: center;
				text-decoration: none;
				display: inline-block;
				margin: 6px 2px;
				cursor: pointer;
				border-radius: 10px;
			}
			
			.propic {
				display: inline-block;
				position: relative;
				float: none;
				width: 256px;
				height: 256px;
				border-radius: 50%;
			}
			
			@media only screen and (max-height: 1080px) and (max-width:1920px) {
				.login {
					font-size: 200%;
				}
				.button {
					font-size: 200%;
					padding: 20px 40px;
					margin: 12px 4px;
					border-radius: 20px;
				}
				.propic {
					width: 256px;
					height: 256px;
				}
			}
			
			@media only screen and (max-height: 1280px) and (max-width:720px) {
				.login {
					font-size: 200%;
				}
				.button {
					font-size: 200%;
					padding: 20px 40px;
					margin: 12px 4px;
					border-radius: 20px;
				}
				.propic {
					width: 256px;
					height: 256px;
				}
			}
			
			@media only screen and (max-height: 720px) and (max-width:1280px) {
				.login {
					font-size: 150%;
				}
				.button {
					padding: 15px 30px;
					margin: 9px 3px;
					border-radius: 15px;
					font-size: 150%;
				}
				.propic {
					width: 128px;
					height: 128px;
				}
			}
			
			@media only screen and (max-height: 360px) and (max-width:640px) {
				.login {
					font-size: 100%;
				}
				.button {
					padding: 10px 20px;
					margin: 6px 2px;
					border-radius: 10px;
					font-size: 100%;
				}
				.propic {
					width: 75px;
					height: 75px;
				}
			}
			
		</style>
		<center id="login-button" class="login">
			<?php
			$bot_username = $_SESSION['bot'];
			$login = json_decode($_COOKIE['tg_user'], true);
			if (isset($login)) {
				if (isset($login['hash'])) {
					$_COOKIE['tg_user'] = json_decode($_COOKIE['tg_user'], true);
					unset($_COOKIE['tg_user']['hash']);
					$_COOKIE['tg_user'] = json_encode($_COOKIE['tg_user']);
					unset($login['hash']);
				}
				$_SESSION['tg_user'] = $login;
				$tg_user = $_SESSION['tg_user'];
			} else {
				unset($_SESSION['tg_user']);
				unset($_COOKIE['tg_user']);
			}
			if ($tg_user['id']) {
				$name = htmlspecialchars($tg_user['first_name']);
				if ($tg_user['last_name']) {
					$name .= " " . htmlspecialchars($tg_user['last_name']);
				}
				if (!isset($tg_user['photo_url'])) {
					$tg_user['photo_url'] = "https://telegra.ph/file/c3f39608b762a4e98b013.png";
				}
				if (isset($tg_user['photo_url'])) {
					$photo_url = htmlspecialchars($tg_user['photo_url']);
					$html .= "<img class=\"propic\" src=\"" . $photo_url . "\" alt=\"$name\">";
				}
				if (isset($tg_user['username'])) {
					$username = htmlspecialchars($tg_user['username']);
					$html .= "<div id=\"telegram-login-widget\">Logged in as <a href=\"https://t.me/$username\">$name</a>!";
				} else {
					$html .= "<div id=\"telegram-login-widget\">Logged in as $name!";
				}
				$html .= "<br><button class=\"button\" onclick=\"onTelegramLogOut()\">Log out</button></div>";
			} else {
				$html = "<div id=\"telegram-login-widget\"><script async src=\"https://telegram.org/js/telegram-widget.js?6\" data-telegram-login=$bot_username data-size=\"large\" data-radius=\"10\" data-onauth=\"onTelegramAuth(user)\" data-request-access=\"write\"></script></div><p></p><div id=\"login-text\">Log in our Web Site for vote, create and manage all polls you want.</div>";
			}
			echo "
						<header>
							<h1>Login for <a href=\"https://t.me/$bot_username\">@$bot_username</a></h1><br>$html
						</header>";
			?>
		</center>
		<!--div align="center" id="cookies">
			cookie: ...
		</div!-->
		<?php
			if (isset($_COOKIE['tg_user'])) {
				$user = json_decode($_COOKIE['tg_user'], true);
				if (is_array($user) and $user['id']) {
					unset($user['hash']);
					$_SESSION['tg_user'] = $user;
					$_COOKIE['tg_user'] = json_encode($user);
				} else {
					unset($_SESSION['tg_user']);
				}
			}
			if (isset($_GET['thread'])) {
				if ($_GET['thread'] == "index") {
					$url = "https://masterpoll.xyz";
				} elseif ($_GET['thread'] == "testing") {
					$url = "https://masterpoll.xyz/testing/";
				} elseif ($_GET['thread'] == "translations") {
					$url = "https://web.masterpoll.xyz/translations/";
				} elseif (isset($_GET['thread'])) {
					$url = "https://masterpoll.xyz/thread.php?p=" . $_GET['thread'];
				} else {
					$url = "https://web.masterpoll.xyz";
				}
				if ($_SESSION['web_redirect']) {
					unset($_SESSION['web_redirect']);
					echo '<meta http-equiv="refresh" content="0;URL=' . $url . '">';
				} else {
					$_SESSION['web_redirect'] = true;
					echo '<div align="center"><button id="back" class="button" onclick="document.location.reload()"><i class="fa fa-chevron-circle-left"></i> Back</button></div>';
				}
			} else {
				$url = "https://web.masterpoll.xyz";
				unset($_GET['thread']);
			}
		?>
		<script>
			/*function init() {
				document.getElementById('cookies').innerHTML = "cookie: " + document.cookie;
				setTimeout(init, 1000);
			}*/
			function onTelegramAuth(e){try{document.cookie="tg_user="+JSON.stringify(e),error=!1}catch(e){error=e.message}error?(document.getElementById("telegram-login-widget").innerHTML="Error: "+error,alert(error)):(e.last_name?e.name=e.first_name+" "+e.last_name:e.name=e.first_name,alert("Logged in as "+e.name+(e.username?" (@"+e.username+")":0)),e.username&&(e.name="<a href='https://t.me/"+e.username+"'>"+e.name+"</a>"),document.getElementById("telegram-login-widget").innerHTML="Logged in as "+e.name+'<br><button class="button" onclick="onTelegramLogOut()">Log out</button>',document.location.reload())}
			function onTelegramLogOut() {
				document.cookie = 'tg_user=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/;domain=;masterpoll.xyz';
				document.getElementById('telegram-login-widget').innerHTML = 'Logged out!<meta http-equiv="refresh" content="0;URL=https://web.masterpoll.xyz/?logout=' + "<?php if (isset($_SESSION['thread'])) echo $_SESSION['thread']; else echo "login_index"; ?>" + '">';
				alert('Logged out');
			}
			init();
		</script>
	</body>
</html>
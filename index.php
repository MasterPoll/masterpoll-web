<?php
include("/home/masterpoll-documents/website/file_names.php");
include($f['config']);

ini_set('error_log', $f['logs']);
if (isset($_COOKIE['PHPSESSID'])) {
	session_id($_COOKIE['PHPSESSID']);
} else {
	setcookie('PHPSESSID', session_id(), 0, '/', "masterpoll.xyz");
}
ini_set("session.cookie_domain", "masterpoll.xyz");
session_start();
if (is_array($_SESSION['tg_user']) and $_SESSION['tg_user']['id']) {
	if (file_exists($f['database']['connection'])) {
		@require($f['database']['connection']);
	} else {
		$error = 404;
		$error_description = "database options not found";
		@require($f['display_web_errors']);
		die;
	}
	$_COOKIE['tg_user'] = json_encode($_SESSION['tg_user']);
	setcookie("tg_user", $_COOKIE['tg_user'], 0, '/', "masterpoll.xyz");
} else {
	unset($_COOKIE['tg_user']);
	unset($_COOKIE['expires']);
	unset($_SESSION['tg_user']);
	setcookie("tg_user", null, time() - 3600, '/', "masterpoll.xyz");
}

$request = explode('/', $_SERVER['REQUEST_URI']);
$dir = explode("?", $request[1])[0];
if (in_array($dir, ['', 'index', 'index.php', 'index.html'])) {
	if (file_exists($f['web']['index'])) {
		@require($f['web']['index']);
	} else {
		$error = 500;
		$error_description = "index file not found";
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: 500 Internal Server Error");
		} else {
			header("HTTP/1.1 500 Internal Server Error");
		}
		http_response_code($error);
		@require($f['display_web_errors']);
	}
} elseif (strpos($dir, "_") === 0) {
	$error = 401;
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 401 Access denied");
	} else {
		header("HTTP/1.1 401 Access denied");
	}
	http_response_code($error);
	@require($f['display_web_errors']);
} else {
	$error = 404;
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 404 Not Found");
	} else {
		header("HTTP/1.1 404 Not Found");
	}
	http_response_code($error);
	@require($f['display_web_errors']);
}

?>

<?php
error_reporting(0);
if (isset($_COOKIE['session_id'])) {
    session_id($_COOKIE['session_id']);
} else {
    setcookie('session_id', session_id(), 0, '/', "masterpoll.cf");
}
ini_set("session.cookie_domain", "masterpoll.cf");
session_start();
if (isset($_SESSION['tg_user'])) {
    if (isset($_SESSION['tg_user']['language_name'])) {
        $_COOKIE['selected-language'] = $_SESSION['tg_user']['language_name'];
    } else {
        $_COOKIE['selected-language'] = "en";
    }
    $_SESSION['login_done'] = true;
} else {
    unset($_COOKIE['selected-language']);
    unset($_COOKIE['tg_user']);
    $_SESSION['login_done'] = false;
}
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Translation's for Master PollBot</title>
</head>
<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
    @media only screen and (orientation: landscape) {
        #turn {
            display: none;
        }

        #leftmenu {
            display: block;
        }

        #page {
            display: block;
        }
    }

    @media only screen and (orientation: portrait) {
        #turn {
            display: none;
        }

        #leftmenu {
            display: block;
        }

        #page {
            display: block;
        }
    }

    @font-face {
        font-family: 'Open Sans', sans-serif;
    }

    h1 {
        font-family: 'Open Sans', sans-serif;
    }

    h2 {
        font-family: 'Open Sans', sans-serif;
    }

    button {
        font-family: 'Open Sans', sans-serif;
    }

    a {
        font-family: 'Open Sans', sans-serif;
        color: rgb(0, 160, 255);
        text-decoration: none;
    }

    strong {
        font-family: 'Open Sans', sans-serif;
    }

    italic {
        font-family: 'Open Sans', sans-serif;
    }

    normal {
        font-family: 'Open Sans', sans-serif;
    }

    code {
        font-family: 'Open Sans', sans-serif;
    }

    json_hidden {
        display: none;
    }

    .navbar {
        border: none;
        margin: 0;
        outline: none;
        font-family: 'Open Sans', sans-serif;
        overflow: hidden;
        background-color: #3399ff;
    }

    .navbar a {
        float: left;
        font-size: 16px;
        color: white;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
    }

    .dropdown {
        float: left;
        overflow: hidden;
    }

    .dropdown .dropbtn {
        font-size: 16px;
        border: none;
        outline: none;
        color: white;
        padding: 14px 16px;
        background-color: inherit;
        font-family: inherit;
        margin: 0;
    }

    .navbar a:hover, .dropdown:hover .dropbtn {
        background-color: #66b3ff;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
    }

    .dropdown-content a {
        float: none;
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        text-align: left;
    }

    .dropdown-content a:hover {
        background-color: #ddd;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    body {
        display: block;
        margin: 0px;
    }

    .telegram-login-navbar {
        float: right;
        background-color: #3399ff;
    }

    .button {
        background-color: #3aa2f8;
        border: none;
        color: white;
        padding: 10px 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        margin: 6px 2px;
        cursor: pointer;
        border-radius: 10px;
    }

    .buttonload {
        background-color: #3aa2f8;
        border: none;
        align: center;
        color: white;
        padding: 10px 20px;
        font-size: 16px
    }

    .languages-list {
        height: 0px;
        width: 100%;
        top: 60px;
        left: 160px;
        border-radius: 0px 0px 8px 8px;
        overflow: hidden;
        z-index: 10000;
        transition: height 0.3s linear 0s;
    }

    .languages-list > span {
        width: 100%;
        top: 60px;
        left: 160px;
        z-index: 10000;
        transition: height 0.3s linear 0s;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="navbar">
    <a href="https://masterpoll.cf"><i class="fa fa-home"></i> Home</a>
    <a href="https://t.me/MasterPoll"><i class="fa fa-info-circle"></i> News</a>
    <div class="dropdown">
        <button class="dropbtn"><i class="fa fa-bars"></i> Clones
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="https://t.me/MasterPoll2Bot">@MasterPoll2Bot</a>
            <a href="https://t.me/NewGroupAgreeBot">@NewGroupAgreeBot</a>
            <a href="https://t.me/MasterPollBetaBot">@MasterPollBetaBot</a>
        </div>
    </div>
    <div class="telegram-login-navbar"><a id="telegram-login" href="https://web.masterpoll.cf?thread=translations"><i
                    class="fa fa-telegram"></i> Login</a></div>
</div>
<div id="turn" align="center">
    <h1>Turn your phone</h1>
    <img src="https://masterpoll.cf/assets/turn_the_phone.png" alt="Turn your phone for view the page!"
         style="width:50%;height:50%;">
</div>
<?php
$_SESSION['thread'] = $_GET['p'];
if (isset($_SESSION['tg_user']['id'])) {
    setcookie("tg_user", json_encode($_SESSION['tg_user']));
} else {
    setcookie("tg_user", null, 1);
}
?>
<script>
    const sleep = (milliseconds) => {
        return new Promise(resolve => setTimeout(resolve, milliseconds));
    };

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    try {
        if (<?php echo $_SESSION['login_done']; ?>) {
            var cookie_user = getCookie('tg_user');
            var user = JSON.parse(cookie_user);
            if (user.id) {
                if (user.last_name) {
                    name = user.first_name + ' ' + user.last_name;
                } else {
                    name = user.first_name;
                }
                document.getElementById('telegram-login').innerHTML = "<i class=\"fa fa-telegram\"></i> " + name.substr(0, 12);
            } else {
                document.getElementById('telegram-login').innerHTML = "<i class=\"fa fa-telegram\"></i> Login";
                document.getElementById('telegram-login').href = "https://web.masterpoll.cf?thread=index";
            }
        } else {
            document.getElementById('telegram-login').innerHTML = "<i class=\"fa fa-telegram\"></i> Login";
            document.getElementById('telegram-login').href = "https://web.masterpoll.cf?thread=index";
        }
    } catch (err) {
        //document.getElementById("telegram-login").innerHTML = err.message;
    }

    function toggleNavPanel(x) {
        var panel = document.getElementById(x), navarrow = document.getElementById("languages-arrow"), maxH = "4100px";
        if (panel.style.height == maxH) {
            panel.style.height = "0px";
            navarrow.innerHTML = '<i class="fa fa-caret-down"></i>';
        } else {
            panel.style.height = maxH;
            navarrow.innerHTML = '<i class="fa fa-caret-up"></i>';
        }
    }
</script>
<body>
<div class="w3-sidebar w3-light-grey w3-bar-block" id="leftmenu" style="width:25%">
    <h3 class="w3-bar-item">Menu</h3>
    <a onclick="toggleNavPanel('languages-list')" class="w3-bar-item w3-button">Languages <span id="languages-arrow"><i
                    class="fa fa-caret-down"></i></span></a>
    <div id="languages-list" class="languages-list">
        <?php
        $languages = json_decode(file_get_contents("languages_name.json"), true);
        foreach ($languages as $language => $name) {
            $name[0] = strtoupper($name[0]);
            echo "<span class=\"3-bar-item w3-button\" onClick=\"selectLanguage($language)\">" . $name . "</span><br>";
        }
        ?>
    </div>
</div>
<div class="page" id="page" style="margin-left:25%;">
    <h1 class="w3-container" align="center">Translate Master Poll</h1>
    <div id="help" align="center" class="w3-container">
        <p><i>Select your language and help our to translate.</i></p>
    </div>
    <div id="text" align="center" class="w3-container">
        <button class="button"><i class="fa fa-spinner fa-spin"></i> Loading...</button>
    </div>
    <script>
        try {
            const sleep = (milliseconds) => {
                return new Promise(resolve => setTimeout(resolve, milliseconds));
            };

            function getCookie(cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            var login = <?php echo json_encode($_SESSION['login_done']); ?>;
            var text = "Language: " + getCookie('selected-language');
            sleep(3000).then(() => {
                if (login) {
                    document.getElementById('text').innerHTML = text;
                } else {
                    document.getElementById('text').innerHTML = "Login to translate the Bot";
                }
            })
        } catch (err) {
        }
    </script>
</div>
<script src="https://cdn.jsdelivr.net/npm/emojione@4.0.0/lib/js/emojione.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/emojione@4.0.0/extras/css/emojione.min.css"/>
<script>
    document.getElementById('text').innerHTML = emojione.toImage(document.getElementById('text').innerHTML);
</script>
</body>
</html>

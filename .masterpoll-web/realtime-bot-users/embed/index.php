<html>
<head>
    <title>Master Poll Subscribers</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <meta name="fragment" content="!"/>
    <meta name="viewport" content="width=300px, initial-scale=1.0">
    <script type="5bd6d241e6aa27b79ed79817-text/javascript">
			var isEmbed = 1;


    </script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,600" rel="stylesheet">
    <link href="../assets/css/style-embed.css" rel="stylesheet">
    <link href="../assets/plugins/odometer/odometer-theme.css" rel="stylesheet">
</head>
<body>
<div id="embed">
    <div class="embedImage">
        <img src="https://telegra.ph/file/1f2c8223aa4dde874b542.jpg" alt="" height="80px" width="80px"
             id="bot-picture"/>
    </div>
    <div class="embedContainer" id="tg_subs">
        <div class="embedName emSec">
            <span class="name">Master Poll</span>
        </div>
        <div class="embedCounter">
            <h2 class="count_live">
                <div id="livesubs" class="count">
                    <?php
                    $ch = curl_init();
                    $url = "https://api.masterpoll.cf/getSubscribers";
                    curl_setopt_array($ch, [
                        CURLOPT_URL => $url,
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
                        CURLOPT_RETURNTRANSFER => true
                    ]);
                    $output = curl_exec($ch);
                    curl_close($ch);
                    $r = json_decode($output, true);
                    echo number_format($r['result']);
                    ?>
                </div>
            </h2>
        </div>
    </div>
</div>
<script>
    const sleep = (milliseconds) => {
        return new Promise(resolve => setTimeout(resolve, milliseconds));
    };

    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    function animateValue(id, end, duration) {
        // assumes integer values for start and end
        var obj = document.getElementById(id);
        var start = Math.round(obj.innerHTML.toString().replace(",", ""));
        if (isNaN(end)) end = Math.round('0');
        if (isNaN(start)) start = Math.round('0');
        var range = end - start; // no timer shorter than 50ms (not really visible any way)
        var minTimer = 50; // calc step time to show all interediate values
        var stepTime = Math.abs(Math.floor(duration / range)); // never go below minTimer
        stepTime = Math.max(stepTime, minTimer); // get current time and calculate desired end time
        var startTime = new Date().getTime();
        var endTime = startTime + duration;
        var timer;

        function run() {
            var now = new Date().getTime();
            var remaining = Math.max((endTime - now) / duration, 0);
            var value = Math.round(end - (remaining * range));
            obj.innerHTML = formatNumber(value);
            if (value == end) {
                clearInterval(timer);
            }
        }

        timer = setInterval(run, stepTime);
        run();
    }

    function animateColor(id, redstart, greenstart, bluestart, redend, greenend, blueend, duration) {
        var zero = Math.round('0');
        var red = redstart;
        var green = greenstart;
        var blue = bluestart;
        var obj = document.getElementById(id);
        obj.style.color = "rgb(" + redend + ", " + greenend + ", " + blueend + ")";
        if (redend < redstart) {
            var redrange = redend - redstart;
            var rede = redend;
        } else if (redend > redstart) {
            var redrange = redstart - redend;
            var rede = redstart;
        } else {
            var redrange = zero;
            var rede = zero;
        }
        if (greenend < greenstart) {
            var greenrange = greenend - greenstart;
            var greene = greenend;
        } else if (greenend > greenstart) {
            var greenrange = greenstart - greenend;
            var greene = greenstart;
        } else {
            var greenrange = zero;
            var greene = zero;
        }
        if (blueend < bluestart) {
            var bluerange = blueend - bluestart;
            var bluee = blueend;
        } else if (blueend > bluestart) {
            var bluerange = bluestart - blueend;
            var bluee = bluestart;
        } else {
            var bluerange = zero;
            var bluee = zero;
        }
        var minTimer = 50;
        var stepTime = Math.abs(Math.floor(duration / 255));
        stepTime = Math.max(stepTime, minTimer); // get current time and calculate desired end time
        var startTime = new Date().getTime();
        var endTime = startTime + duration;
        var timer;

        function run() {
            var now = new Date().getTime();
            var remaining = Math.max((endTime - now) / duration, 0);
            if (rede == redstart) {
                var red = Math.round(redend + (remaining * redrange));
            }
            if (rede == redend) {
                var red = Math.round(rede - (remaining * redrange));
            }
            if (greene == greenstart) {
                var green = Math.round(greenend + (greenrange * remaining));
            }
            if (greene == greenend) {
                var green = Math.round(greene - (remaining * greenrange));
            }
            if (bluee == bluestart) {
                var blue = Math.round(blueend + (remaining * bluerange));
            }
            if (bluee == blueend) {
                var blue = Math.round(bluee - (remaining * bluerange));
            }
            obj.style.color = "rgb(" + red + ", " + green + ", " + blue + ")";
            if (red == redend && green == greenend && blue == blueend) {
                obj.style.color = "rgb(" + redend + ", " + greenend + ", " + blueend + ")";
                clearInterval(timer);
            }
        }

        timer = setInterval(run, stepTime);
        run();
    }

    function readTextFile() {
        var rawFile = new XMLHttpRequest();
        rawFile.open("GET", "https://api.masterpoll.cf/getSubscribers", true);
        rawFile.timeout = 3000;
        rawFile.ontimeout = function (e) {
            animateColor("livesubs", 250, 0, 0, 0, 0, 0, 1000);
        };
        rawFile.onreadystatechange = function () {
            if (rawFile.readyState === 4) {
                if (rawFile.status === 200 || rawFile.status == 0) {
                    var allText = rawFile.responseText;
                    var json = JSON.parse(allText);
                    try {
                        if (json.result == document.getElementById("livesubs").innerHTML.toString().replace(",", "")) {
                            animateColor("livesubs", 0, 0, 250, 0, 0, 0, 1000);
                        } else if (json.error_code) {
                            animateColor("livesubs", 60, 60, 0, 255, 255, 0, 1000);
                            document.getElementById("livesubs").innerHTML = "API Error...";
                            sleep(1500).then(() => {
                                animateColor("livesubs", 255, 255, 0, 60, 60, 0, 2500);
                            })
                        } else {
                            animateColor("livesubs", 0, 0, 0, 0, 255, 0, 1000);
                            animateValue("livesubs", json.result, 1000);
                            sleep(1000).then(() => {
                                animateColor("livesubs", 0, 255, 0, 0, 0, 0, 2000);
                            })
                        }
                        sleep(3000).then(() => {
                            document.getElementById("livesubs").innerHTML = formatNumber(json.result);
                        })
                    } catch (err) {
                        animateColor("livesubs", 60, 60, 0, 255, 255, 0, 1000);
                        document.getElementById("livesubs").innerHTML = "API Error...";
                        sleep(1500).then(() => {
                            animateColor("livesubs", 255, 255, 0, 60, 60, 0, 2500);
                        })
                    }
                } else {
                    document.getElementById("livesubs").innerHTML = "NO";
                }
            } else {
                document.getElementById("livesubs").innerHTML = "NO";
            }
        };
        rawFile.send(null);
    }

    function init() {
        try {
            animateColor("livesubs", 0, 255, 255, 0, 0, 0, 1000);
            readTextFile();
            sleep(8000).then(() => {
                animateColor("livesubs", 0, 0, 0, 0, 255, 255, 2000);
            })
        } catch (err) {
            animateColor("livesubs", 0, 0, 0, 255, 0, 0, 1000);
            document.getElementById("livesubs").innerHTML = "Offline:" + err.message;
            sleep(1500).then(() => {
                document.getElementById("livesubs").innerHTML = "Offline..";
            });
            sleep(2000).then(() => {
                document.getElementById("livesubs").innerHTML = "Offline...";
                animateColor("livesubs", 255, 0, 0, 0, 0, 0, 2000);
            })
        }
        setTimeout(init, 10000);
    }

    init();
</script>
</body>
</html>

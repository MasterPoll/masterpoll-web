TG.query = {
    newSearch: function (e) {
        if (e.trim() == TG.live.channelID || e.trim() == "") {
            return;
        }
        TG.live.stop();
        if (e.trim().substr(0, 2).toUpperCase() == "UC" && e.trim().length >= 24) {
            $.getJSON("https://api.masterpoll.cf/getSubscribers", function (e) {
                TG.live.start();
                if (e.pageInfo.totalResults < 1) {
                    alert("No results found!");
                    location.href = baseURL;
                    return;
                }
                var gsnippet = e.items[0];
                TG.updateManager.updateChannelID(gsnippet.id);
                TG.query.getCover(gsnippet.id);
                TG.updateManager.updateName(gsnippet.snippet.title);
                TG.updateManager.updateProfile(gsnippet.snippet.thumbnails.high.url ? gsnippet.snippet.thumbnails.high.url : gsnippet.snippet.thumbnails.default.url);
                TG.urls.pushState(gsnippet.id);
            });
        } else {
            $.getJSON("https://www.googleapis.com/youtube/v3/search?part=snippet&q=" + encodeURIComponent(e) + "&type=channel&maxResults=1&key=" + TG.keyManager.getKey(), function (e) {
                TG.live.start();
                if (e.pageInfo.totalResults < 1) {
                    alert("No results found!");
                    location.href = baseURL;
                    return;
                }
                var snippet = e.items[0].snippet;
                TG.updateManager.updateChannelID(snippet.channelId);
                TG.query.getCover(snippet.channelId);
                TG.updateManager.updateName(snippet.channelTitle);
                TG.updateManager.updateProfile(snippet.thumbnails.high.url ? snippet.thumbnails.high.url : snippet.thumbnails.default.url);
                TG.urls.pushState(snippet.channelId);
            });
        }
    },
    getCover: function (e) {
        $.getJSON("https://www.googleapis.com/youtube/v3/channels?part=brandingSettings&id=" + encodeURIComponent(e) + "&key=" + TG.keyManager.getKey(), function (e) {
            TG.updateManager.updateCover(e.items[0].brandingSettings.image.bannerImageUrl);
        });
    },
    search: function (e) {
        e.preventDefault();
        TG.query.newSearch($("#yt_searchvalue").val());
        $("#yt_searchvalue").val("");
    },
    bind: function () {
        $("#yt_search").on("submit", this.search);
        $("#yt_searchbutton").on("click", this.search);
    }
};
TG.live = {
    channelID: "",
    update: function () {
        $.getJSON("https://api.masterpoll.cf/getSubscribers", function (e) {
            if (e.pageInfo.totalResults > 0) {
                TG.updateManager.updateSubscribers(e.items[0].statistics.subscriberCount);
                TG.updateManager.updateViews(e.items[0].statistics.viewCount);
                TG.updateManager.updateVideos(e.items[0].statistics.videoCount);
            } else {
                TG.query.newSearch(TG.live.channelID);
            }
        });
    },
    timer: null,
    start: function () {
        this.stop();
        this.timer = setInterval(function (e) {
            TG.live.update();
        }, (isEmbed) ? 5000 : 1500);
        TG.live.update();
    },
    stop: function () {
        clearInterval(this.timer);
    }
};
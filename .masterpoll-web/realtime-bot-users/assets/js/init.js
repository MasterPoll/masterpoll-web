$(window).bind("popstate", TG.urls ? TG.urls.onchange : false);
$(function () {
    TG.updateManager.prepare();
    TG.sharing.bind();
    TG.multisearch.bind();
    TG.query.bind();
    TG.pins && TG.pins.getPins();
    TG.urls && TG.urls.onchange();
});
var disqus_config = function () {
    this.page.url = "";
    this.page.identifier = "";
};
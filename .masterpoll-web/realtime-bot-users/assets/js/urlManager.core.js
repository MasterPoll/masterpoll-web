TG.urls = {
    onchange: function () {
        var q = location.hash.split("!/")[1];
        if (q) {
            TG.query.newSearch(location.hash.split("!/")[1]);
        } else {
            var coolGuys = ['UCHkj014U2CQ2Nv0UZeYpE_A', 'UCBJycsmduvYEL83R_U4JriQ', 'UCtinbF-Q-fVthA0qrFQTgXQ', 'UCp0hYYBW6IMayGgR-WeoCvQ', 'UCBJycsmduvYEL83R_U4JriQ'];
            TG.query.newSearch(coolGuys[Math.floor(Math.random() * coolGuys.length)]);
        }
    },
    pushState: function (e) {
        history.pushState(null, null, "#!/" + e);
        DISQUS.reset({
            reload: true,
            config: function () {
                this.page.identifier = e;
                this.page.url = baseURL + "#!/" + e;
            }
        });
        TG.query.newSearch(e);
    },
    getCurrent: function () {
        return baseURL + "#!/" + TG.live.channelID;
    }
};
<?php
error_reporting(0);
if (isset($_COOKIE['session_id'])) {
    session_id($_COOKIE['session_id']);
} else {
    setcookie('session_id', session_id(), 0, '/', "masterpoll.cf");
}
ini_set("session.cookie_domain", "masterpoll.cf");
session_start();
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Telegram Login for Master Poll</title>
</head>
<body>
<?php
if ($_GET['bot']) {
    $cloni = ["MasterPollBot", "MasterPoll2Bot", "NewGroupAgreeBot", "MasterPollBetaBot"];
    if (!in_array($_GET['bot'], $cloni)) {
        $bot = "MasterPollBot";
    } else {
        $bot = $_GET['bot'];
    }
} else {
    if (!isset($_SESSION['bot'])) {
        $bot = "MasterPollBot";
    } else {
        $bot = $_SESSION['bot'];
    }
}
$_SESSION['bot'] = $bot;
?>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    h1 {
        font-family: 'Open Sans', sans-serif;
    }

    a {
        font-family: 'Open Sans', sans-serif;
        color: rgb(0, 160, 255);
        text-decoration: none;
    }

    div {
        font-family: 'Open Sans', sans-serif;
    }

    center {
        font-family: 'Open Sans', sans-serif;
    }

    .button {
        background-color: #3aa2f8;
        border: none;
        color: white;
        padding: 10px 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        margin: 6px 2px;
        cursor: pointer;
        border-radius: 10px;
    }
</style>
<center id="login-button">
    <?php
    $bot_username = $_SESSION['bot'];
    function getTelegramUserData()
    {
        if (isset($_COOKIE['tg_user'])) {
            $auth_data_json = urldecode($_COOKIE['tg_user']);
            $auth_data = json_decode($auth_data_json, true);
            return $auth_data;
        }
        return false;
    }

    $tg_user = getTelegramUserData();
    $_SESSION['tg_user'] = $tg_user;
    if ($tg_user !== false) {
        $name = htmlspecialchars($tg_user['first_name']);
        if ($tg_user['last_name']) {
            $name .= " " . htmlspecialchars($tg_user['last_name']);
        }
        if (isset($tg_user['photo_url'])) {
            $photo_url = htmlspecialchars($tg_user['photo_url']);
            $html .= "<img src=\"{$photo_url}\">";
        }
        if (isset($tg_user['username'])) {
            $username = htmlspecialchars($tg_user['username']);
            $html = "<div id=\"telegram-login-widget\">Logged in as <a href=\"https://t.me/$username\">$name</a>!";
        } else {
            $html = "<div id=\"telegram-login-widget\">Logged in as $name!";
        }
        $html .= "<br><button class=\"button\" onclick=\"onTelegramLogOut()\">Log out</button></div>";
    } else {
        $html = "<div id=\"telegram-login-widget\"><script async src=\"https://telegram.org/js/telegram-widget.js?6\" data-telegram-login=$bot_username data-size=\"large\" data-radius=\"10\" data-onauth=\"onTelegramAuth(user)\" data-request-access=\"write\"></script></div>";
    }
    echo "
						<header>
							<h1>Login for <a href=\"https://t.me/$bot_username\">@$bot_username</a></h1><br>$html
						</header>";
    ?>
    <p></p>
    <div id="login-text">
        Log in our Web Site for vote, create and manage all polls you want.
    </div>
</center>
<script>
    function onTelegramAuth(user) {
        document.cookie = 'tg_user=' + JSON.stringify(user);
        if (user.last_name) {
            user.name = user.first_name + ' ' + user.last_name;
        } else {
            user.name = user.first_name;
        }
        alert('Logged in as ' + user.name + (user.username ? ' (@' + user.username + ')' : +''));
        if (user.username) {
            user.name = "<a href='https://t.me/" + user.username + "'>" + user.name + "</a>";
        }
        document.getElementById('telegram-login-widget').innerHTML = "Logged in as " + user.name + "<br><button class=\"button\" onclick=\"onTelegramLogOut()\">Log out</button>";
        document.location.reload();
    }

    function onTelegramLogOut() {
        document.cookie = "tg_user=nothing; expires = Thu, 01 Jan 1970 00:00:00 GMT";
        document.getElementById('telegram-login-widget').innerHTML = "Logged out!";
        alert('Logged out');
        document.location.reload();
    }
</script>
<?php
if (!$_SESSION['tg_user'] and $_COOKIE['tg_user']) {
    $_SESSION['tg_user'] = json_decode($_COOKIE['tg_user'], true);
    $redirectgo = false;
} elseif ($_SESSION['tg_user']) {
    $redirectgo = false;
} else {
    $redirectgo = false;
}
if (isset($_GET['thread'])) {
    if ($_GET['thread'] == "index") {
        $url = "https://masterpoll.cf";
    } elseif ($_GET['thread'] == "translations") {
        $url = "https://web.masterpoll.cf/translations/";
    } else {
        $url = "https://masterpoll.cf/thread.php?p=" . $_GET['thread'];
    }
    if ($_SESSION['web_redirect']) {
        unset($_SESSION['web_redirect']);
        echo '<meta http-equiv="refresh" content="0;URL=' . $url . '">';
    } else {
        $_SESSION['web_redirect'] = true;
        echo '<div align="center"><button id="back" class="button" onclick="document.location.reload()"><i class="fa fa-chevron-circle-left"></i> Back</button></div>';
    }
}
?>
</body>
</html>
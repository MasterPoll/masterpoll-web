<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<title>Master Poll Subscribers</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<meta name="fragment" content="!" />
		<meta name="viewport" content="initial-scale=1.0">
		<script type="text/javascript">
			var isEmbed = 1;
		</script>
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,600" rel="stylesheet">
		<link href="../assets/css/style-embed.css" rel="stylesheet">
	</head>
	<body>
		<div id="embed">
			<div class="embedImage">
				<img src="https://telegra.ph/file/70657fca868e26e8ff19f.jpg" alt="MasterPoll Logo" height="80px" width="80px" id="bot-picture" />
			</div>
			<div class="embedContainer" id="tg_subs">
				<div class="embedName emSec">
					<span class="name">Master Poll</span>
				</div>
				<div class="embedCounter">
					<h2 class="count_live">
						 <div id="livesubs" class="count">
							<?php 
								$ch = curl_init();
								$url = "https://api.masterpoll.xyz/getSubscribers";
								curl_setopt_array($ch, [
									CURLOPT_URL => $url,
									CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
									CURLOPT_RETURNTRANSFER => true
								]);
								$output = curl_exec($ch);
								curl_close($ch);
								$r = json_decode($output, true);
								echo number_format($r['result']);
							?>
						</div>
					</h2>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="../assets/js/subscribers-count.js"></script>
	</body>
</html>
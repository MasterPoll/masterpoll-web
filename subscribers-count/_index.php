<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<title>Master Poll Subscribers</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<meta name="fragment" content="!" />
		<meta name="viewport" content="initial-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,600" rel="stylesheet">
		<!--link href="assets/css/style.css" rel="stylesheet"!-->
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131336731-7"></script>
		<script>
  			window.dataLayer = window.dataLayer || [];
  			function gtag(){dataLayer.push(arguments);}
  			gtag('js', new Date());

  			gtag('config', 'UA-131336731-7');
		</script>

	</head>
	<body>
		<style>
		@import url("https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700");
		html body {
			overflow: hidden;
			color: black;
			font-family: "Roboto", sans-serif;
		}

		* {
			padding: 0;
			margin: 0;
		}

		.container {
			overflow: hidden;
		}
		
		.image {
			position: relative;
			border-radius: 30px;
		}
		.name {
			font-weight: 100;
			font-size: 22px;
			margin-top: 5px;
		}
		.counter {
			position: relative;
		}
		.counter .count_live {
			font-size: 38px;
			margin: 0;
		}
		.page {
			display: -webkit-flexbox;
			display: -ms-flexbox;
			-ms-flex-align: center;
			text-align: center;
			align-items: center;
			justify-content: center;
		}
		@media only screen and (max-height: 1080px) {
			.image {
				height: 512px;
				width: 512px;
			}
			.name {
				font-size: 44px;
				margin-top: 10px;
			}
			.counter .count_live {
				font-size: 76px;
			}
		}
		@media only screen and (max-height: 720px) {
			.image {
				height: 256px;
				width: 256px;
			}
			.name {
				font-size: 22px;
				margin-top: 5px;
			}
			.counter .count_live {
				font-size: 38px;
			}
		}
		@media only screen and (max-height: 360px) {
			.image {
				height: 128px;
				width: 128px;
			}
			.name {
				font-size: 11px;
				margin-top: 2px;
			}
			.counter .count_live {
				font-size: 19px;
			}
		}
		@media only screen and (max-height: 180px) {
			.image {
				height: 64px;
				width: 64px;
			}
			.name {
				font-size: 5px;
				margin-top: 1px;
			}
			.counter .count_live {
				font-size: 10px;
			}
		}
	</style>
		<div class="page">
			<img class="image" src="https://telegra.ph/file/70657fca868e26e8ff19f.jpg" alt="MasterPoll Logo" height="150px" width="150px" id="bot-picture"/>
			<div class="container" id="tg_subs">
				<span class="name">Master Poll</span>
				<div class="counter">
					<h2 class="count_live">
						 <div id="livesubs" class="count">
							<?php 
								$ch = curl_init();
								$url = "https://api.masterpoll.xyz/getSubscribers";
								curl_setopt_array($ch, [
									CURLOPT_URL => $url,
									CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
									CURLOPT_RETURNTRANSFER => true
								]);
								$output = curl_exec($ch);
								curl_close($ch);
								$r = json_decode($output, true);
								echo number_format($r['result']);
							?>
						</div>
					</h2>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="assets/js/subscribers-count.js"></script>
	</body>
</html>
